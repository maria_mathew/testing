package test;
public class passwordValidator {

	public static void main (String [] args)
	{
		System.out.println("Is kelly a valid password: "+ isValid("Kelly"));
		System.out.println("Is password a valid password: "+ isValid("password"));
		System.out.println("Is helenk56 a valid password: "+ isValid("helenk56"));
		System.out.println("Is hu77 a valid password: "+ isValid("Kelly"));
	}
	
	public static boolean isValid(String password)
	{
		boolean isValidLength = false;
		boolean isValidCharactor = false;
		int digitCount = 0;
		
		if(password.length()>=8)
		{
			isValidLength = true;
		}
		
		for(int i= 0; i<password.length(); i++)
		{
			if(Character.isDigit(password.charAt(i)))
				digitCount++;
		}
		if(digitCount>=2)
			isValidCharactor = true;
		
		if(isValidCharactor == true && isValidLength == true)
		{
			return true;
		}
		
		else
		return false;
	}
	
}
