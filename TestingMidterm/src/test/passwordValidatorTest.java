package test;

import static org.junit.Assert.*;

import org.junit.Test;

public class passwordValidatorTest {

	@Test
	public void testIsValid() {
		boolean result = passwordValidator.isValid("password44");
		assertTrue("Correct!", result == true);
	}
	@Test
	public void testIsValidException() {
		boolean result = passwordValidator.isValid("pword44");
		assertFalse("Correct!", result == true);
	}
	@Test
	public void testIsValidBoundaryIn() {
		boolean result = passwordValidator.isValid("pworxd44");
		assertTrue("Correct!", result == true);
	}
	@Test
	public void testIsValidBoundaryOut() {
		boolean result = passwordValidator.isValid("pwrdd44");
		assertFalse("Correct!", result == true);
	}

}
